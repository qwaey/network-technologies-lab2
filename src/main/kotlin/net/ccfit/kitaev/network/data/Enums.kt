package net.ccfit.kitaev.network.data

enum class LauncherOptions {
    UNKNOWN_OPTION,
    ONLY_CLIENT,
    ONLY_SERVER
}