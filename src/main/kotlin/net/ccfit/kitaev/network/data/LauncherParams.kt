package net.ccfit.kitaev.network.data

import java.io.File
import java.net.InetAddress

data class LauncherParams(
        val options: LauncherOptions = LauncherOptions.UNKNOWN_OPTION,
        val ip: InetAddress = InetAddress.getLocalHost(),
        val port: Int = 80,
        val file: File = File.createTempFile("temp_file", "txt")
)