package net.ccfit.kitaev.network.domain.parsers

import net.ccfit.kitaev.network.data.FileNotExist
import net.ccfit.kitaev.network.data.LauncherOptions
import net.ccfit.kitaev.network.data.LauncherParams
import java.io.File
import java.net.InetAddress

class InputParser {

    companion object {
        fun parse(args: Array<String>): LauncherParams {
            return when (args.size) {
                1 -> LauncherParams(getOptions(args[0]))
                2 -> LauncherParams(getOptions(args[0]), getAddress(args[1]))
                3 -> LauncherParams(getOptions(args[0]), getAddress(args[1]), getPort(args[2]))
                4 -> LauncherParams(getOptions(args[0]), getAddress(args[1]), getPort(args[2]), getFile(args[3]))
                else -> LauncherParams()
            }
        }

        private fun getOptions(options: String): LauncherOptions {
            return LauncherOptions.valueOf(options)
        }

        private fun getAddress(addressName: String): InetAddress {
            return InetAddress.getByName(addressName)
        }

        private fun getPort(port: String): Int {
            return port.toInt()
        }

        private fun getFile(filename: String): File {
            val file = File(filename)
            if (!file.exists()) {
                throw FileNotExist()
            }
            return file
        }
    }
}