package net.ccfit.kitaev.network.launcher

import net.ccfit.kitaev.network.data.LauncherOptions
import net.ccfit.kitaev.network.data.UnknownOptions
import net.ccfit.kitaev.network.domain.parsers.InputParser
import net.ccfit.kitaev.network.main.client.Client
import net.ccfit.kitaev.network.main.server.Server

fun main(args: Array<String>) {
    val params = InputParser.parse(args)
    when (params.options) {
        LauncherOptions.ONLY_CLIENT -> Client(params).start()
        LauncherOptions.ONLY_SERVER -> Server(params).start()
        else -> throw UnknownOptions()
    }
}