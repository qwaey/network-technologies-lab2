package net.ccfit.kitaev.network.main.client

import net.ccfit.kitaev.network.data.LauncherParams
import net.ccfit.kitaev.network.main.SharedConstants
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket
import java.util.*
import kotlin.math.min

class Client(private val params: LauncherParams) : Thread("ClientMainThread") {

    override fun run() {
        try {
            val socket = Socket(params.ip, params.port)
            send(DataOutputStream(socket.getOutputStream()))
            receiveAnswer(DataInputStream(socket.getInputStream()))
            socket.close()
        } catch (e: Exception) {
            println(e.localizedMessage)
        }
    }

    private fun send(outputStream: DataOutputStream) {
        val inputStream = DataInputStream(params.file.inputStream())
        val buffer = ByteArray(SharedConstants.BUFFER_SIZE)

        outputStream.writeUTF(UUID.randomUUID().toString())
        outputStream.writeUTF(params.file.name)
        outputStream.writeLong(params.file.length())

        var sendBytes = 0L
        while (sendBytes < params.file.length()) {
            var size = min(params.file.length() - sendBytes, SharedConstants.BUFFER_SIZE.toLong()).toInt()
            size = inputStream.read(buffer, 0, size)
            outputStream.write(buffer, 0, size)
            sendBytes += size
        }

        inputStream.close()
    }

    private fun receiveAnswer(inputStream: DataInputStream) {
        print(inputStream.readUTF())
    }
}