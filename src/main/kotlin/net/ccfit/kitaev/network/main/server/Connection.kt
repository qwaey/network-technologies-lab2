package net.ccfit.kitaev.network.main.server

import net.ccfit.kitaev.network.main.SharedConstants
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.net.Socket
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class Connection(private val socket: Socket, private val rootFolder: Path) : Thread("ServerConnection") {

    private val printDelay = 3L
    private var uuid = ""

    override fun run() {
        val inputStream = DataInputStream(socket.getInputStream())
        val outputStream = DataOutputStream(socket.getOutputStream())
        val file = createFile(inputStream)
        val totalSize = receiveFile(file, inputStream)
        sendAnswer(file, totalSize, outputStream)
        socket.close()
    }

    private fun createFile(inputStream: DataInputStream): File {
        uuid = inputStream.readUTF()
        val filename = inputStream.readUTF()

        val folderPath = createFolder(rootFolder, uuid)
        val filePath = Paths.get(filename).fileName.toString()

        return File(folderPath + filePath)
    }

    private fun createFolder(root: Path, uuid: String): String {
        val pathToUuidFolder = Paths.get(root.toString(), uuid)
        return Files.createDirectory(pathToUuidFolder).toString() + "\\"
    }

    private fun receiveFile(file: File, inputStream: DataInputStream): Long {
        val outputStream = DataOutputStream(file.outputStream())

        val fileSize = inputStream.readLong()
        val buffer = ByteArray(SharedConstants.BUFFER_SIZE)

        val speedTester = SpeedTester(printDelay)
        speedTester.start()

        var receiveBytes = 0L
        while (receiveBytes < fileSize) {
            val size = inputStream.read(buffer, 0, SharedConstants.BUFFER_SIZE)
            outputStream.write(buffer, 0, size)

            receiveBytes += size
            speedTester.currentRead += size
            speedTester.check(uuid, file.name, fileSize)
        }

        speedTester.finish(uuid, file.name, fileSize)
        outputStream.close()
        return fileSize
    }

    private fun sendAnswer(file: File, totalSize: Long, outputStream: DataOutputStream) {
        val str = "Successfully accepted (${file.length()}/$totalSize) bytes"
        outputStream.writeUTF(str)
    }
}