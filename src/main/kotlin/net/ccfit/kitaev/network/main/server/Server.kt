package net.ccfit.kitaev.network.main.server

import net.ccfit.kitaev.network.data.LauncherParams
import java.net.ServerSocket
import java.nio.file.Files
import java.nio.file.Paths

class Server(private val params: LauncherParams) : Thread("ServerMainThread") {

    override fun run() {
        val path = Paths.get("C:\\Users\\Andrey\\Git\\network-technologies-lab2\\Uploads")
        val folder = Files.createDirectories(path)

        val serverSocket = ServerSocket(params.port)

        while (Thread.currentThread().isAlive) {
            val clientSocket = serverSocket.accept()
            Connection(clientSocket, folder).start()
        }

        serverSocket.close()
    }
}