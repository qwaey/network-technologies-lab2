package net.ccfit.kitaev.network.main.server

import java.time.ZonedDateTime

class SpeedTester(private val printDelay: Long) {

    private var startTime: Long = 0
    private var currentTime: Long = 0
    var totalRead: Long = 0
    var currentRead: Long = 0

    fun start() {
        startTime = ZonedDateTime.now().toEpochSecond()
        currentTime = startTime
        currentRead = 0
        totalRead = 0
    }

    fun check(uuid: String, filename: String, fileSize: Long) {
        if (ZonedDateTime.now().toEpochSecond() - currentTime >= printDelay) {
            totalRead += currentRead
            printResult(uuid, filename, fileSize, printDelay)
            currentTime = ZonedDateTime.now().toEpochSecond()
            currentRead = 0
        }
    }

    fun finish(uuid: String, filename: String, fileSize: Long) {
        val delay = ZonedDateTime.now().toEpochSecond() - currentTime
        totalRead += currentRead
        printResult(uuid, filename, fileSize, delay)
        println("$uuid finished\n")
    }

    private fun printResult(uuid: String, filename: String, fileSize: Long, delay: Long) {
        val totalReceivedPercent = 100 * totalRead / fileSize

        val momentumSpeed = when (delay.toInt()) {
            0 -> currentRead / (delay + 1) / 1024
            else -> currentRead / delay / 1024
        }

        val averageSpeed = when ((currentTime - startTime).toInt()) {
            0 -> totalRead / (currentTime - startTime + 1) / 1024
            else -> totalRead / (currentTime - startTime) / 1024
        }

        val stringBuilder = StringBuilder()
        val string = stringBuilder.append("$uuid: $filename\n")
                .append("Received: $totalReceivedPercent%\n")
                .append("Momentum speed: $momentumSpeed KB/Sec\n")
                .append("Average speed: $averageSpeed KB/Sec\n")
        println(string)
    }
}