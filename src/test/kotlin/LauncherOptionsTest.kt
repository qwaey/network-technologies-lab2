
import net.ccfit.kitaev.network.data.LauncherOptions
import net.ccfit.kitaev.network.domain.parsers.InputParser
import org.junit.Assert
import org.junit.Test

class LauncherOptionsTest {
    @Test
    fun singleTest() {
        Assert.assertEquals(LauncherOptions.ONLY_CLIENT, InputParser.parse(arrayOf("ONLY_CLIENT")).options)
    }
}